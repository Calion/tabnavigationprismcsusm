﻿using System;
using System.Diagnostics;
using Prism;
using Prism.Mvvm;

namespace PrismTabNav1.ViewModels
{
    public class TabCViewModel : BindableBase, IActiveAware
    {

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public event EventHandler IsActiveChanged;

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { SetProperty(ref _isActive, value); }
        }

        public TabCViewModel()
        {
            Title = "Tab C";
            IsActiveChanged += OnIsActiveChanged;
        }

        void OnIsActiveChanged(object sender, EventArgs emptyEventArgs)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnIsActiveChanged)}: {IsActive}");
        }
    }
}
