﻿using System;
using System.Diagnostics;
using Prism.Mvvm;
using Prism.Navigation;

namespace PrismTabNav1.ViewModels
{
    public class TabContainerPageViewModel : BindableBase, INavigationAware
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public TabContainerPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabContainerPageViewModel)}:  ctor");
            Title = "Tab Container Page";
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }
    }
}
