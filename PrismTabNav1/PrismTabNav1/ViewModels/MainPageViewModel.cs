﻿using System;
using System.Diagnostics;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using PrismTabNav1.Views;

namespace PrismTabNav1.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {

        INavigationService _navigationService;

        public DelegateCommand NavToTabContainerCommand { get; set; }


        public MainPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavToTabContainerCommand = new DelegateCommand(OnNavToTabContainer);
        }

        private void OnNavToTabContainer()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavToTabContainer)}");

            _navigationService.NavigateAsync(nameof(TabContainerPage));
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedFrom)}");
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatedTo)}");
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}");
        }
    }
}
